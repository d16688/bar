/*
 * @Author: your name
 * @Date: 2021-02-20 14:41:46
 * @LastEditTime: 2021-02-23 22:48:38
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \bar\bar\流动人口界面\wangba\js\pie.js
 */
d3.json("./data/chaoshi_tu4.json", function (error, data) {
    bar_id="50011210000249"
    
    console.log(data)
    console.log(data[bar_id])
    setInterval(function () {
        bar_id=window.bar_name
        // console.log(paga_para_name);
        if (bar_id == "") {

        }
        else {
            var dom = document.getElementById("tu1-4");
            var myChart = echarts.init(dom);
            data_basic=data[bar_id]
            data_arr=[]
            dict1={},dict2={},dict3={},dict4={}
            dict1["name"]="18及以下"
            dict1["value"]=data_basic["18及以下"]
            dict2["name"]="18-25"
            dict2["value"]=data_basic["18-25"]
            dict3["name"]="25-35"
            dict3["value"]=data_basic["25-35"]
            dict4["name"]="35及以上"
            dict4["value"]=data_basic["35及以上"]  
            data_arr[0]=dict1
            data_arr[1]=dict2
            data_arr[2]=dict3
            data_arr[3]=dict4
            // console.log(data_arr)
            // valuedata[name][0]["总人数"]
            var option = {
                title:{
                    text:bar_id,
                    subtext:'年龄比例',
                    textStyle:{
                        color:'#fff',
                    },
                    bottom:'5%',
                    subtextStyle:{
                        color:'#fff',
                    }
                },
                color:['#fdf1f1','#eb3223','#7bd7f5','rgb(117, 102, 247)'],
                tooltip: {
                    trigger: 'item'
                },
                legend: {
                    top: '25%',
                    right:'34%',
                    textStyle: {
                        color: 'white'
                    },
                    orient:'vertical'
                },
                series: [
                    {
                        type: 'pie',
                        radius: ['40%', '70%'],
                        center:['33%','50%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 10,
                            borderColor: '#fff',
                            borderWidth: 2
                        },
                        label: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: '40',
                                fontWeight: 'bold'
                            }
                        },
                        labelLine: {
                            show: false
                        },
                        data: data_arr
                    }
                ]
            };


            if (option && typeof option === 'object') {
                myChart.setOption(option);
                
            }
            myChart.on('click',function(params){
                // console.log(params.data.name);
                page_para_age=params.data.name
                console.log(page_para_age)
            });
        }
    }, 1000);
})
