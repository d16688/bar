/*
 * @Author: your name
 * @Date: 2021-02-21 22:31:38
 * @LastEditTime: 2021-02-23 22:19:18
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \bar (2)\bar\bar\流动人口界面\wangba\js\rili.js
 */
d3.json('./data/chaoshi_tu3.json', (function (Data) {
    setInterval(function () {
    var chartDom = document.getElementById('tu1-3');
    var myChart = echarts.init(chartDom);
    var option;
    console.log(Data[window.bar_name])
    var times = Object.keys(Data[window.bar_name]);
    var valArr = times.map(function (i) { return Data[window.bar_name][i] });
    console.log(times)
    console.log(valArr)

    function getVirtulData(year) {
        year = year || '2016';
        var date = +echarts.number.parseDate(year + '-10-01');
        var end = +echarts.number.parseDate((+year + 1) + '-12-31');
        var dayTime = 3600 * 24 * 1000;
        var data = [];
        for (var i = 0; i < times.length; i++) {
            for (var time = date; time < end; time += dayTime) {
                if (time = times[i]) {
                    data.push([
                        echarts.format.formatTime('yyyy-MM-dd', time),
                        valArr[i]
                    ]);
                }
            }
        }
        return data;
    }
    option = {
        title:{
            text:window.bar_name,
            textStyle:{
                color:'#fff'
            }
        },
        tooltip: {
        },
        calendar: {
            top: 'middle',
            left: 'center',
            orient: 'horizontal',
            cellSize: 27,
            yearLabel: {
                margin: 50,
                color: '#fff',
                textStyle: {
                    fontSize: 30
                }
            },
            dayLabel: {
                firstDay: 1,
                nameMap: 'cn'
            },
            monthLabel: {
                nameMap: 'cn',
                margin: 10,
                fontSize: 14,
                color: '#fff'
            },
            range: ['2016-10', '2016-12-31']
        },
        visualMap: {
            min: 0,
            max: 150,
            type: 'piecewise',
            left: 'center',
            bottom: 20,
            inRange: {
                color: ['#5291FF', '#C7DBFF']
            },
            seriesIndex: [1],
            orient: 'horizontal',
            textStyle: {
                color: '#fff'
            }
        },
        series: [{
            type: 'graph',
            edgeSymbol: ['none', 'arrow'],
            coordinateSystem: 'calendar',
            symbolSize: 15,
            calendarIndex: 0,
            z: 20
        }, {
            type: 'heatmap',
            coordinateSystem: 'calendar',
            data: getVirtulData(2016)
        }]
    };

    option && myChart.setOption(option);
    console.log(window.bar_name)
}, 1000);
}))