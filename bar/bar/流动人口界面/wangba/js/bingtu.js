/*
 * @Author: dzb
 * @Date: 2021-01-27 16:28:56
 * @LastEditTime: 2021-02-19 21:08:33
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \地域数据\wangba\wangba\js\bingtu.js
 */
d3.json("./data/zongti_tu2.json", function (error, data) {
    setInterval(function () {
        // console.log(paga_para_name);
        if (page_para_name == "") {

        }
        else {
            var dom = document.getElementById("tu1-2");
            var myChart = echarts.init(dom);
            data_basic=data[page_para_name][0]
            data_arr=[]
            dict1={},dict2={},dict3={},dict4={}
            dict1["name"]="18及以下"
            dict1["value"]=data_basic["18及以下"]
            dict2["name"]="18-25"
            dict2["value"]=data_basic["18-25"]
            dict3["name"]="25-35"
            dict3["value"]=data_basic["25-35"]
            dict4["name"]="35及以上"
            dict4["value"]=data_basic["35及以上"]  
            data_arr[0]=dict1
            data_arr[1]=dict2
            data_arr[2]=dict3
            data_arr[3]=dict4
            // console.log(data_arr)
            // valuedata[name][0]["总人数"]
            var option = {
                title:{
                    text:page_para_name,
                    subtext:'年龄比例',
                    textStyle:{
                        color:'#fff',
                    },
                    bottom:'5%',
                    subtextStyle:{
                        color:'#fff',
                    }
                },
                color:['#fdf1f1','#eb3223','#7bd7f5','rgb(117, 102, 247)'],
                tooltip: {
                    trigger: 'item'
                },
                legend: {
                    top: '5%',
                    right:'34%',
                    textStyle: {
                        color: 'white'
                    }
                },
                series: [
                    {
                        name: '访问来源',
                        type: 'pie',
                        radius: ['40%', '70%'],
                        center:['33%','50%'],
                        avoidLabelOverlap: false,
                        itemStyle: {
                            borderRadius: 10,
                            borderColor: '#fff',
                            borderWidth: 2
                        },
                        label: {
                            show: false,
                            position: 'center'
                        },
                        emphasis: {
                            label: {
                                show: true,
                                fontSize: '40',
                                fontWeight: 'bold'
                            }
                        },
                        labelLine: {
                            show: false
                        },
                        data: data_arr
                    }
                ]
            };


            if (option && typeof option === 'object') {
                myChart.setOption(option);
                
            }
            myChart.on('click',function(params){
                // console.log(params.data.name);
                page_para_age=params.data.name
                console.log(page_para_age)
            });
        }
    }, 1000);
})