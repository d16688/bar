/*
 * @Author: your name
 * @Date: 2021-01-28 12:16:42
 * @LastEditTime: 2021-02-19 21:07:46
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \wangba (2)\wangba\js\bar.js
 */
d3.json('./data/zongti_tu3.json', (function (data) {
    // 参数是page_para_age
    setInterval(function () {
        if (page_para_name == "浙江" || page_para_name == "重庆") {

        }
        if (page_para_age == "") {

        }
        else {
            console.log(page_para_name)
            console.log(page_para_age)
            var mychart = echarts.init(document.querySelector('.tu1-3'))
            var countrys = Object.keys(data);
            num = countrys.indexOf(page_para_name);
            var valArr = countrys.map(function (i) { return data[i] });
            if (page_para_age == "18及以下") {
                var subArr1 = valArr[num]["18岁及以下"]["total"]
                var subArr2 = valArr[num]["18岁及以下"]["female"]
                var subArr3 = valArr[num]["18岁及以下"]["male"]
            }
            else {
                var subArr1 = valArr[num][page_para_age]["total"]
                var subArr2 = valArr[num][page_para_age]["female"]
                var subArr3 = valArr[num][page_para_age]["male"]
            }
            var data1_1 = subArr1['0-2h']
            var data2_1 = subArr1['2-5h']
            var data3_1 = subArr1['5-10h']
            var data4_1 = subArr1['10-20h']
            var data5_1 = subArr2['20-max']
            var dataall1 = [data1_1, data2_1, data3_1, data4_1, data5_1]
            var data1_2 = subArr2['0-2h']
            var data2_2 = subArr2['2-5h']
            var data3_2 = subArr2['5-10h']
            var data4_2 = subArr2['10-20h']
            var data5_2 = subArr2['20-max']
            var dataall2 = [data1_2, data2_2, data3_2, data4_2, data5_2]
            var data1_3 = subArr3['0-2h']
            var data2_3 = subArr3['2-5h']
            var data3_3 = subArr3['5-10h']
            var data4_3 = subArr3['10-20h']
            var data5_3 = subArr3['20-max']
            var dataall3 = [data1_3, data2_3, data3_3, data4_3, data5_3]
            option = {
                color:['#eb3223','#fdf1f1','#7bd7f5'],
                tooltip: {
                    axisPointer: {
                        type: 'cross',
                        crossStyle: {
                            color: '#fff',
                        }
                    }
                },
                legend: {
                    data: ['总量', '男性人数', '女性人数'],
                    textStyle:{
                        color:'#fff',
                    },
                },
                toolbox: {
                    show: true,
                    top:'8%',
                    right:'8%',
                    feature: {
                        dataView: { show: true, readOnly: false },
                        magicType: { show: true, type: ['line', 'bar'] },
                        restore: { show: true },
                        saveAsImage: { show: true }
                    },
                    iconStyle:{
                        color:'#fff'
                    }
                },
                calculable: true,
                xAxis: {
                    name:'上网次数',
                    type: 'category',
                    data: ['0-2h', '2-5h', '5-10h', '10-20h', '20-max'],
                    axisLine:{
                        lineStyle:{
                            color:'#fff'
                        }
                    }
                },
                yAxis: {
                    name:'人数',
                    type: 'value',
                    axisLine:{
                        lineStyle:{
                            color:'#fff'
                        }
                    }
                },
                series: [
                    {
                        name: '总量',
                        type: 'bar',
                        data: dataall1,
                        markPoint: {
                            data: [
                                { type: 'max', name: '最大值' },
                                { type: 'min', name: '最小值' }
                            ]
                        },
                        markLine: {
                            data: [
                                { type: 'average', name: '平均值' }
                            ]
                        }
                    },
                    {
                        name: '男性人数',
                        type: 'bar',
                        data: dataall3,
                        markPoint: {
                            data: [
                                { type: 'max', name: '最大值' },
                                { type: 'min', name: '最小值' }
                            ]
                        },
                        markLine: {
                            data: [
                                { type: 'average', name: '平均值' }
                            ]
                        }
                    },
                    {
                        name: '女性人数',
                        type: 'bar',
                        data: dataall2,
                        markPoint: {
                            data: [
                                { type: 'max', name: '最大值' },
                                { type: 'min', name: '最小值' }
                            ]
                        },
                        markLine: {
                            data: [
                                { type: 'average', name: '平均值' }
                            ]
                        }
                    }
                ]
            };
            mychart.setOption(option)
        }
    }, 1000);
}))