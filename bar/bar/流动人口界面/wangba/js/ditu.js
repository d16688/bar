/*
 * @Author: dzb
 * @Date: 2021-01-27 16:18:58
 * @LastEditTime: 2021-02-19 21:09:26
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: \地域数据\wangba\wangba\js\ditu.js
 */

var page_para_name=""
var page_para_age=""
var width = document.querySelector("#tu1-1").offsetWidth;
var height =500;
console.log(document.querySelector("#tu1-1").offsetHeight)
var svg = d3.select("#tu1-1").append("svg")
.attr("width", width)
.attr("height", height);


var projection = d3.geo.mercator()
.center([107, 31])
.scale(550)
.translate([width/2+50, height/2+80]);

var path = d3.geo.path()
.projection(projection);


d3.json("./data/china.topojson", function (error, toporoot) {
if (error)
    return console.error(error);

//输出china.topojson的对象
console.log(toporoot);

//将TopoJSON对象转换成GeoJSON，保存在georoot中
var georoot = topojson.feature(toporoot, toporoot.objects.china);

//输出GeoJSON对象
console.log(georoot);

//包含中国各省路径的分组元素
var china = svg.append("g");

//添加中国各种的路径元素
var provinces = china.selectAll("path")
    .data(georoot.features)
    .enter()
    .append("path")
    .attr("class", "province")
    .style("fill", "#ccc")
    .attr("d", path)
    .on("click",function(d){
        arr={}
        page_para_name=d.properties.name
        arr["province"]=d.properties.name
    })

d3.json("./data/zongti_tu2.json", function (error, valuedata) {
    let province = ["北京", "天津", "河北", "山西", "内蒙古", "辽宁", "吉林", "黑龙江", "上海"
        , "江苏", "浙江", "安徽", "福建", "江西", "山东", "河南", "湖北", "湖南",
        "广东", "广西", "海南", "重庆", "四川", "贵州", "云南", "西藏", "陕西",
        "甘肃", "青海", "宁夏", "新疆"]
    
    var values = [];
    let v_arr = []
    for (item in province) {
        var name = province[item];
        // console.log(name)
        var value = valuedata[name][0]["总人数"]
        // console.log(valuedata[name])
        values[name] = value;
        v_arr[item] = value
    }
    // console.log(values)
    var maxvalue = Math.ceil(Math.pow(d3.max(v_arr, function (d) {
        return d;
    }),0.47));
    // var maxvalue =Math.ceil( Math.log(d3.max(v_arr, function (d) {
    //     return d;
    // })));
    // valuedata.provinces, function(d){ return d.value; }
    var minvalue = 0;

    //定义一个线性比例尺，将最小值和最大值之间的值映射到[0, 1]
    var linear = d3.scale.linear()
        .domain([minvalue, maxvalue])
        .range([0, 1]);

    //定义最小值和最大值对应的颜色
    var a = d3.rgb(	255,255,255);	//粉红色
    var b = d3.rgb(	255,0,0);	//黑色

    //颜色插值函数
    var computeColor = d3.interpolate(a, b);

    //设定各省份的填充色
    provinces.style("fill", function (d, i) {
        var t = linear(Math.pow(values[d.properties.name],0.5));
        var color = computeColor(t);
        return color.toString();
    });

    //定义一个线性渐变
    var defs = svg.append("defs");

    var linearGradient = defs.append("linearGradient")
        .attr("id", "linearColor")
        .attr("x1", "0%")
        .attr("y1", "0%")
        .attr("x2", "100%")
        .attr("y2", "0%");

    var stop1 = linearGradient.append("stop")
        .attr("offset", "0%")
        .style("stop-color", a.toString());

    var stop2 = linearGradient.append("stop")
        .attr("offset", "100%")
        .style("stop-color", b.toString());

    //添加一个矩形，并应用线性渐变
    var colorRect = svg.append("rect")
        .attr("x", 20)
        .attr("y", 450)
        .attr("width", 140)
        .attr("height", 20)
        .style("fill", "url(#" + linearGradient.attr("id") + ")");

    //添加文字
    var minValueText = svg.append("text")
        .attr("class", "valueText")
        .attr("x", 30)
        .attr("y", 490)
        .attr("fill","white")
        .attr("dy", "-0.3em")
        .text(function () {
            return minvalue;
        });

    var maxValueText = svg.append("text")
        .attr("class", "valueText")
        .attr("x", 160)
        .attr("y", 490)
        .attr("fill","white")
        .attr("dy", "-0.3em")
        .text(function () {
            return maxvalue;
        });		
})



});
