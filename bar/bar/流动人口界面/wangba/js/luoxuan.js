d3.json('./data/chaoshi_tu2.json', (function (data) {
    setInterval(function () {
        var mychart = echarts.init(document.querySelector('.tu1-2'))
        var numbs = Object.keys(data);
        var valArr = numbs.map(function (i) { return data[i] });
        var datal = new Array();
        var selnumbs = new Array();

        console.log(window.vnum)
        console.log(selnumbs)
        var vals=window.vnum*0.01
        for (var i = 0; i < numbs.length; i++) {
            if(valArr[i]["zhanbi"]==vals){
                selnumbs.push(numbs[i])
            }
        }

        for (var i = 0; i < numbs.length; i++) {
            if(valArr[i]["zhanbi"]==vals){
                
            var subArr1 = valArr[i]["all"]
            var subArr2 = valArr[i]["weigui"]
            var subArr3 = valArr[i]["zhanbi"]
            var subdata = [subArr1, subArr2, subArr3]
            datal.push(subdata)
            }
        }
        console.log(selnumbs)
        console.log(datal)
        option = {
            color: ['#eb3223', '#7bd7f5'],
            title: {
                text: '超时经营',
                left: '8%',
                textStyle: {
                    color: '#fff'
                }
            },
            legend: {
                show: true,
                data: ['经营总时长', '违规经营时长'],
                textStyle: {
                    color: '#fff',
                },
                right: '35%'
            },
            toolbox: {
                show: true,
                right: '15%',
                feature: {
                    dataView: { show: true, readOnly: false },
                    magicType: { show: true, type: ['line', 'bar'] },
                    saveAsImage: { show: true }
                },
                iconStyle: {
                    color: '#fff'
                }
            },

            grid: {
                top: 100
            },
            angleAxis: {
                type: 'category',
                data: selnumbs,
                show: false
            },
            tooltip: {
                show: true,
                formatter: function (params) {
                    var id = params.dataIndex;
                    return selnumbs[id] + '<br>经营总时长：' + datal[id][0] + '<br>违规经营时长：' + datal[id][1] + '<br>违规经营时长占比：' + datal[id][2];
                }
            },
            radiusAxis: {
                axisLine: {
                    lineStyle: {
                        color: '#fff'
                    }
                }
            },
            polar: {
            },
            series: [
                {
                    type: 'bar',
                    itemStyle: {
                        color: 'transparent'
                    },
                    data: datal.map(function (d) {
                        return d[1];
                    }),
                    coordinateSystem: 'polar',
                    stack: '经营总时长',
                    silent: true
                },
                {
                    type: 'bar',
                    data: datal.map(function (d) {
                        return d[0] - d[1];
                    }),
                    coordinateSystem: 'polar',
                    name: '经营总时长',
                    stack: '经营总时长'
                },
                {
                    type: 'bar',
                    itemStyle: {
                        color: 'transparent'
                    },
                    data: datal.map(function (d) {
                        return 0;
                    }),
                    coordinateSystem: 'polar',
                    stack: '违规经营时长',
                    silent: true,
                    z: 10
                },
                {
                    type: 'bar',
                    data: datal.map(function (d) {
                        return d[1];
                    }),
                    coordinateSystem: 'polar',
                    name: '违规经营时长',
                    stack: '违规经营时长',
                    barGap: '-100%',
                    z: 10
                }]
        };
        mychart.setOption(option)
        mychart.on('click',function(params){
            // console.log(params);
            window.bar_name=params.name
            // console.log(window.bar_name)
        });
    }, 1000);
}))