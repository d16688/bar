/*
 * @Author: frp
 * @Date: 2021-02-18 09:08:59
 * @LastEditTime: 2021-02-19 21:12:19
 * @LastEditors: Please set LastEditors
 * @Description:用高德地图API可视化数据绘制热力地图
 * @FilePath: \网吧违规经营可视化\bar\流动人口界面\wangba\js\realTimeMonitoring\map.js
 */
d3.json('./data/shishi_address.json', (function (data) {

    let map = new AMap.Map('heatMap', {
        features: ['bg', 'road'],
        mapStyle: 'amap://styles/grey',
        center: [106.530635013, 29.5446061089], //重庆市中心经纬度
        zoom: 7,
    });

    let layer = new Loca.HeatmapLayer({
        map: map,
    });

    //预处理数据
    let heatMapData = []

    for (let i = 0; i < 24; i++) {
        heatMapData.push([])

        data[`${i}`].forEach(item => {
            const temp = item.slice(1, item.length - 1)
            heatMapData[i].push({
                "lnglat": temp
            })
        })
    }

    // console.log(heatMapData);


    layer.setOptions({
        style: {
            radius: 8,
            color: {
                0.5: '#2c7bb6',
                0.65: '#abd9e9',
                0.7: '#ffffbf',
                0.9: '#fde468',
                1.0: '#d7191c'
            }
        }
    });


    //动态地每3秒重新渲染地图
    let index = 0
    let id = setInterval(() => {
        layer.setData(heatMapData[index++], {
            lnglat: 'lnglat',
        });
        layer.render();
        if (index >= 24) {
            setTimeout(`clearInterval(${id})`)
        }
    }, 3000)



}))