/*
 * @Author:frp
 * @Date: 2021-02-19 15:52:56
 * @LastEditTime: 2021-02-19 21:12:30
 * @LastEditors: Please set LastEditors
 * @Description: 柱状图
 * @FilePath: \网吧违规经营可视化\bar\流动人口界面\wangba\js\realTimeMonitoring\barChart.js
 */

let barChart = echarts.init(document.getElementById('barChart'));

d3.json('./data/shishi_tu2_3.json', (function (data) {
    let option = {
        tooltip: {
            trigger: 'axis',
            axisPointer: {
                type: 'shadow'
            }
        },
        xAxis: [{
            name: "年龄",
            type: 'category',
            data: ["18以下", "18-25", "25-35", "35以上"],
            axisTick: {
                alignWithLabel: true
            }
        }],
        yAxis: [{
            name: "上网次数",
            type: 'value'
        }],
        series: [{
            type: 'bar',
            barWidth: '60%',
            data: []
        }]
    };

    let index = 0
    const id = setInterval(() => {
        option.series[0].data = [data["18以下"][`${index}`], data["18-25"][`${index}`], data["25-35"][`${index}`], data["35以上"][`${index}`]]
        index++
        barChart.setOption(option)
        if (index >= 24) {
            setTimeout(`clearInterval(${id})`)
        }
    }, 3000)
}))