/*
 * @Author: frp
 * @Date: 2021-02-18 20:58:18
 * @LastEditTime: 2021-02-19 21:12:51
 * @LastEditors: Please set LastEditors
 * @Description: 折线图
 * @FilePath: \网吧违规经营可视化\bar\流动人口界面\wangba\js\realTimeMonitoring\lineChart.js
 */

let lineChart = echarts.init(document.getElementById('lineChart'));

d3.json('./data/shishi_tu2_3.json', (function (d) {
    const data = d["total"]

    let option = {
        title: {
            text: "各时间段上网人数情况",
        },
        xAxis: {
            name: "时间",
            type: 'category',
            boundaryGap: false,
            data: Object.keys(data)
        },
        yAxis: {
            name: "上网人数",
            type: 'value',
        },
        tooltip: {
            show: true,
            trigger: 'axis',
        },
        series: [{
            name: '上网人数',
            data: [],
            smooth: false,
            type: 'line'
        }]
    };

    let index = 0
    const id = setInterval(() => {
        option.series[0].data.push(data[`${index++}`])
        lineChart.setOption(option)
        if (index >= 24) {
            setTimeout(`clearInterval(${id})`)
        }
    }, 3000)

}))